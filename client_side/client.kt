/*
How to compile & launch this program : 

kotlinc client.kt -include-runtime -jvm-target 1.8 -d client.jar
java -jar client.jar

*/

import java.io.File
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse.BodyHandlers
import java.net.URI
import java.nio.file.Path

fun main() {
    val image_path = "image.png";
    val url_path = "url";

    // Read the url
    val url = File(url_path).readText(Charsets.UTF_8);

    println("Image will be sent to : " + url);
    
    // Build the client
    val client = HttpClient.newBuilder().build();

    // Build the request
    val request = HttpRequest.newBuilder()
        .header("Content-Type", "image")
        .POST(HttpRequest.BodyPublishers.ofFile(Path.of(image_path)))
        .uri(URI.create(url))
        .build();


    // Get the response and print it
    val response = client.send(request, BodyHandlers.ofString());
    println(response.body())
}


/*

Basically a copy/paste from https://handyopinion.com/upload-file-to-server-in-android-kotlin/

 

implementation("com.squareup.okhttp3:okhttp:4.9.0")
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

fun main() {
    val image_path = "image.png";
    val url_path = "url";

    // Read the url
    val serverURL = File(url_path).readText(Charsets.UTF_8);

    val mimeType = getMimeType(sourceFile);
    if (mimeType == null) {
        println("file error : Not able to get mime type")
        return
    }
    val fileName: String = image_path
    toggleProgressDialog(true)
    try {
        val requestBody: RequestBody =
            MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("image", fileName,sourceFile.asRequestBody(mimeType.toMediaTypeOrNull()))
                .build()

        val request: Request = Request.Builder().url(serverURL).post(requestBody).build()

        val response: Response = client.newCall(request).execute()

        if (response.isSuccessful) {
            println("File uploaded successfully at $serverUploadDirectoryPath$fileName")
        } else {
            println("File uploading failed")
        }
    } catch (ex: Exception) {
        println("File uploading failed")
    }
}
*/
