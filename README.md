# ML Smartphone Server

Implementation of this protocol :
- A smartphone sends an image to a web server.
- The web server analyses the image (for example with machine learning).
- The web server sends a response