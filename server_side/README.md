# Server side folder

## How to run the API

To run the API (source code : [api.py](./api.py)) :

1. Run [install_python_and_flask.sh](install_python_and_flask.sh) if it is not installed yet.

2. Run [run.sh](run.sh).
