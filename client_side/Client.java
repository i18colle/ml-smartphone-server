import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client{

    private static CloseableHttpClient client = HttpClients.createDefault();

    public static void main(String args[]) throws URISyntaxException, IOException {
        String image_path = "image.png";
        String ip_address_path = "ip_address";
        String endpoint = "/analyseimage";

        String ip_address = readTextFile(ip_address_path);
        String url_string = "http://"+ip_address+endpoint;
        URI uri= new URI(url_string);
        File image_file=new File(image_path);
        String response = uploadImage(image_file, uri);
        System.out.println("Response :\n\n"+response);
    }

    // This code is inspired by https://gist.github.com/kjlubick/70aea8d38831777360c0, consulted on Dec 13 2020.

    private static String uploadImage(File file, URI uri) throws IOException {
        
        // This response handler is from https://www.javaguides.net/2019/07/java-http-getpost-request-example.html
        ResponseHandler < String > responseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }
        };
        
        System.out.println("Connection to server : "+uri);
        HttpPost httpPost = new HttpPost(uri);
        String response="";
        try {
            MultipartEntityBuilder mpeBuilder = MultipartEntityBuilder.create();
            mpeBuilder.addBinaryBody("image", file);
            HttpEntity content = mpeBuilder.build();
            httpPost.setEntity(content);
            response = client.execute(httpPost, responseHandler);
        } catch(Exception e ){
            System.out.println("Error : the file could not be upload.");
        } finally {
            httpPost.reset();
            return response;
        }
    }

    // From https://www.w3schools.com/java/java_files_read.asp
    private static String readTextFile(String path){
        try {
            File file = new File(path);
            Scanner myReader = new Scanner(file);
            String data="";
            while (myReader.hasNextLine()) {
              data += myReader.nextLine();
            }
            myReader.close();
            return data;
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return null;
        }
    }

 }