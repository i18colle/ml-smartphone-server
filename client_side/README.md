# Client side folder

## How to run the program

To run the client program (source code : [Client.java](./Client.java)) :

1. Run [install_jre.sh](install_jre.sh) if it is not installed yet.

2. Write the server's ip address in [ip_address](ip_address).

3. Run [run.sh](run.sh).

## Kotlin code

I tried to do the program with kotlin ([client.kt](client.kt) and [client.jar](client.jar)). But I kept getting an error, so I switched to java.

