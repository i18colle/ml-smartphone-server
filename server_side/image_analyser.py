from PIL import Image

#Return the first and last pixels
def analyse(path):
    try :
        im = Image.open(path).getdata() #Load the image as a list of tuples
        im = list(im)
        return {
            "First pixel" : str(im[0]),
            "Last pixel" : str(im[len(im)-1]) 
        }
    except :
        return "Error in the analyse of image"