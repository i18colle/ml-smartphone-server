import flask, os
from flask import request, jsonify
from image_analyser import analyse as analyse_image

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['upload_folder']="./images"

@app.route('/', methods=['GET', 'POST'])
def home():
    return '''
    <h1>This site is for research purpose</h1>
    <p>To send an image and receive an answer, use the '/analyseimage' endpoint.</p>'''

'''How to use this endpoint with curl (=on a linux terminal):
curl -X POST -F 'image=@[path to the image]' http://[ip address]/analyseimage
'''
@app.route('/analyseimage', methods=['POST'])
def api_analyseimage():

    #Store the image as 'images/[hash_of_the_image].[extension]
    try :
        file = request.files['image']
    except :
        if len(request.files)==1 :
            file = request.files[
                list(request.files.keys())[0]   # File = the only element in request.files
            ]
        else :
            return "Error : Image not found."
    try :
        extension = os.path.splitext(file.filename)[-1]
        filename=str(hash(file))+extension
        filepath = os.path.join(app.config['upload_folder'], filename)
        file.save(filepath)

        # Analyse the image
        result=analyse_image(filepath)

        # Remove the image and send back the result
        os.remove(filepath)
        return jsonify(result)
    except :
        return("Error while analyzing the image.")

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

app.run(host = '0.0.0.0', port=80)